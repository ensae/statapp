On a quelques paramètres sur lesquels jouer.

- n_ctx (**128**, 256, 512)
- n_embd (256, 512, **768**, 1024)
- N (**10**, 100, 500)
- n_layer (**8**)
- n_head (**8**)

Contrainte : n_head et n_layer doivent être des diviseurs de n_embd.

À faire en plus : produire de quoi évaluer les modèles entraînés. 
- Sous-tâche 1 : disposer d'une liste standardisée de prompts
- Sous-tâche 2 : disposer d'une ou plusieurs métriques de performance (perplexité, mais pas que).

Ensuite, on enchaîne.

Taile TOP 10: environ 1,2 GO
Taille TOP 100: environ 5,8 GO
Taille TOP 500: environ 14,2 GO
