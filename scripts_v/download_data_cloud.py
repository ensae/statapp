### ARCHIVE ###


### download des data sur le cloud ###

!pip install --quiet datasets 

from datasets import load_dataset, Dataset

import os
import s3fs
import random
import json

data = load_dataset('wikipedia', '20220301.en', split="train")

# Mettre ici votre ID Datalab
BUCKET_OUT = "vincentg"  

S3_ENDPOINT_URL = "https://" + os.environ["AWS_S3_ENDPOINT"]
fs = s3fs.S3FileSystem(client_kwargs={'endpoint_url': S3_ENDPOINT_URL})
fs.ls(BUCKET_OUT)

SEED_VALUE = 3797823 # Grand nombre tiré aléatoirement par un PRNG.
size = len(data)

def perm( size,seed_value=SEED_VALUE):
    random.seed(seed_value)
    permutation = random.sample(range(size), size)
    return permutation

def dump_perm(permutation):
    FILE_PERM_OUT_S3 = BUCKET_OUT + "/" + "StatApp/permutation.json"
    with fs.open(FILE_PERM_OUT_S3, 'w') as f:
        json.dump(permutation, f)


%%time

permutation = perm(size)
dump_perm(permutation)
shuffled_data = [data[i] for i in permutation]

%%time 

taille_sous_liste = 10000
sous_listes = [shuffled_data[i:i+taille_sous_liste] for i in range(0, size, taille_sous_liste)]

for i, sous_liste in enumerate(sous_listes):
    FILE_CHUNK_SHUFFLED_OUT_S3 = BUCKET_OUT + "/" + f"StatApp/chunk_{i}_shuffled_wikipedia_data.json"
    with fs.open(FILE_CHUNK_SHUFFLED_OUT_S3, 'w') as f:
        json.dump(sous_liste, f)
        print(i, end=" ")              # Pour vérifier que cela ne crash pas

def upload_data(filepath, file_name):
    s3 = s3fs.S3FileSystem()
    s3_path = f"glevy/Modèles/Test/{file_name}"
    s3.put(filepath, s3_path, recursive=True)



#### Download des datasets (top x sur 10000) tokenized #####


# Choisir un Tokenizer
tokenizer = AutoTokenizer.from_pretrained("gpt2-medium")
#tokenizer = GPT2Tokenizer.from_pretrained('gpt2-medium')

def tokenize(element, tokenizer=None, context_length=128):
    outputs = tokenizer(
        element["text"],
        truncation=True,
        max_length= context_length,
        return_overflowing_tokens=True,
        return_length=True,
    )
    input_batch = []
    for length, input_ids in zip(outputs["length"], outputs["input_ids"]):
        if length == context_length:
            input_batch.append(input_ids)
    return {"input_ids": input_batch}




def tokenized_to_cloud(context_length=128, user = "vincentg", file="top_10_sur_10000.json", top ="10"):
    with fs.open(user + "/StatApp/" + file , 'r') as f:
        data = Dataset.from_list(json.load(f))
    
    tokenized_datasets = data.map(
        lambda element: tokenize(element, tokenizer=tokenizer),
        batched=True,remove_columns=data.column_names)
    FILE_CHUNK_SHUFFLED_OUT_S3 = user + "/StatApp/" +f"tokenized_{context_length}_top" + top + ".json"
    with fs.open(FILE_CHUNK_SHUFFLED_OUT_S3, 'w') as f:
    #json.dump(tokenized_datasets, f)
    #tokenized_datasets.to_json(f)
        json.dump(tokenized_datasets['input_ids'],f)
    print("check")


## Puis il reste à exécuter ça. Ca prend du temps donc faites le tourner tout seul avec %% capture

%%capture captured_output
tokenized_to_cloud(context_length=128, user = "vincentg", file="top_100_sur_10000.json")
tokenized_to_cloud(context_length=256, user = "vincentg", file="top_100_sur_10000.json")
tokenized_to_cloud(context_length=512, user = "vincentg", file="top_100_sur_10000.json")
tokenized_to_cloud(context_length=128, user = "vincentg", file="top_100_sur_10000.json", top = "100")
tokenized_to_cloud(context_length=256, user = "vincentg", file="top_100_sur_10000.json", top = "100")
tokenized_to_cloud(context_length=512, user = "vincentg", file="top_100_sur_10000.json", top = "100")
tokenized_to_cloud(context_length=128, user = "vincentg", file="top_500_sur_10000.json", top= "500")
tokenized_to_cloud(context_length=256, user = "vincentg", file="top_500_sur_10000.json", top = "500")
tokenized_to_cloud(context_length=512, user = "vincentg", file="top_500_sur_10000.json", top= "500")