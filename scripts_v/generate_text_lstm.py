# scripts/generate_text_lstm.py
import torch
from transformers import GPT2Tokenizer

def generate_text_lstm(model_lstm, tokenizer, seed_text, length):
    model_lstm.eval()

    input_ids_lstm = tokenizer.encode(seed_text, return_tensors="pt")

    for _ in range(length):
        with torch.no_grad():
            output_lstm = model_lstm(input_ids_lstm)

        next_token_lstm = torch.argmax(output_lstm[:, -1, :]).item()
        next_token_lstm = torch.tensor([[next_token_lstm]])
        input_ids_lstm = torch.cat([input_ids_lstm, next_token_lstm], dim=-1)

    generated_text_lstm = tokenizer.decode(input_ids_lstm[0], skip_special_tokens=True)
    return generated_text_lstm

# Generate text using the trained model
with torch.no_grad():
    model.eval()
    starting_text = "This is"
    starting_text_indices = torch.tensor(vectorizer.transform([starting_text]).toarray(), dtype=torch.float32)
    generated_text_indices = []

    for _ in range(10):  # Generate 10 tokens
        output = model(starting_text_indices)
        _, predicted_index = torch.max(output, 2)
        generated_text_indices.append(predicted_index[:, -1].item())
        starting_text_indices = torch.cat([starting_text_indices, predicted_index[:, -1].unsqueeze(0).float()], dim=1)

    # Convert back to words
    generated_text = vectorizer.inverse_transform(generated_text_indices)
    print("Generated Text:", " ".join(generated_text[0]))
