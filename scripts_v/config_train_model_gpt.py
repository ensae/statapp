# script config_and_train_model_gpt
from transformers import Trainer, TrainingArguments, AutoTokenizer
from datasets import load_dataset, DatasetDict
from transformers import AutoConfig, GPT2LMHeadModel, DataCollatorForLanguageModeling
import os
import pandas as pd
import s3fs


def tokenize(element, context_length=128):
    outputs = tokenizer(
        element['text'],
        truncation=True,
        max_length=context_length,
        return_overflowing_tokens=True,
        return_length=True,
    )
    input_batch = []
    for length, input_ids in zip(outputs["length"], outputs["input_ids"]):
        if length == context_length:
            input_batch.append(input_ids)
    return {'input_ids':input_batch}


def config_train_model_gpt(name, tokenized_datasets, tokenizer=None, n_embd=512, n_layer=8, n_head=8, GPU=False, learning_rate=5e-4, epochs=2, context_length=128,  fs=None, USER=None):

    config = AutoConfig.from_pretrained(
    "gpt2",
    vocab_size=len(tokenizer),
    n_embd= n_embd,
    n_layer= n_layer,
    n_head= n_head,
    n_ctx=context_length,
    bos_token_id=tokenizer.bos_token_id,
    eos_token_id=tokenizer.eos_token_id
    )

    model = GPT2LMHeadModel(config)

    model_size = sum(t.numel() for t in model.parameters())
    print(f"GPT-2 size: {model_size/1000**2:.1f}M parameters")
    
    tokenizer.pad_token = tokenizer.eos_token
    data_collator = DataCollatorForLanguageModeling(tokenizer, mlm=False)
    
    args = TrainingArguments(
    output_dir= name,     
    per_device_train_batch_size=32,           
    per_device_eval_batch_size=32,           
    evaluation_strategy="steps",
    eval_steps=5_000,
    logging_steps=5_000,
    gradient_accumulation_steps=8,
    num_train_epochs= epochs,
    weight_decay=0.1,
    warmup_steps=1_000,
    lr_scheduler_type="cosine",
    learning_rate= learning_rate,
    save_steps=5_000,
    fp16= GPU,  # <- à modifier selon si vous êtes en GPU (fp16=True en GPU)
        
    )
        
    trainer = Trainer(
    model=model,
    tokenizer=tokenizer,
    args=args,
    data_collator=data_collator,
    train_dataset=tokenized_datasets["train"],
    eval_dataset=tokenized_datasets["valid"])
    
    trainer.train()

    # Parce que oui, entraîner sans sauvegarder c'est pas malin.
    model.save_pretrained(f'./saved_model_{name}')
    tokenizer.save_pretrained(f'./saved_model_{name}')
    
    fs.put(f'./saved_model_{name}', f'{USER}/Modèles/{name}', recursive=True)

    return pd.DataFrame(trainer.state.log_history), model

   






 


    
    