# Statapp 'Petits modèles de langue'

Thème numéro 49, année 2023-2024.

Par Matéo Amazo, Romain Delhommais, Vincent Gimenes, Guillaume Lévy.

## Licence

Ce projet est sous la licence GPLv3.
