import random

SEED_VALUE = 3797823 # Valeur par défaut choisie par la RNG?

def perm(seed_value=SEED_VALUE, size):
    random.seed(seed_value)
    permutation = random.sample(range(size), len(size))
    return permutation

def dump_perm(permutation):
    FILE_PERM_OUT_S3 = BUCKET_OUT + "/" + "StatApp/permutation.json"
    with fs.open(FILE_PERM_OUT_S3, 'w') as f:
        json.dump(permutation, f)


def shuffle_data(permutation, data): #ajouter le chemin en argument
    shuffled_data = [data[i] for i in permutation]
    
    FILE_SHUFFLED_OUT_S3 = BUCKET_OUT + "/" + "StatApp/shuffled_wikipedia_data.json"
    with fs.open(FILE_SHUFFLED_OUT_S3, 'w') as f:
        json.dump(shuffled_data, f)


        # Flemme d'en faire quelque chose pour l'instant, de toute façon c'est à usage unique en principe.
        # De l'archivage, en somme.
# taille_sous_liste = 10000
# sous_listes = [shuffled_wikipedia_data[i:i+taille_sous_liste] for i in range(0, len(shuffled_wikipedia_data), taille_sous_liste)]

# for i, sous_liste in enumerate(sous_listes):
#     FILE_CHUNK_SHUFFLED_OUT_S3 = BUCKET_OUT + "/" + f"StatApp/chunk_{i}_shuffled_wikipedia_data.json"
#     with fs.open(FILE_CHUNK_SHUFFLED_OUT_S3, 'w') as f:
#         json.dump(sous_liste, f)


# Dans le même genre, voilà comment sélectionner les plus grosses pages des chunks pour en faire un nouveau dataset.


def select_top_n_dicts(file_path, n):
    with fs.open(file_path, 'r') as f:
        data = json.load(f)

    # Trier les dictionnaires en fonction de la longueur de la valeur associée à 'text'
    sorted_data = sorted(data, key=lambda x: len(x['text']), reverse=True)

    # Sélectionner les N premiers dictionnaires
    selected_dicts = sorted_data[:n]

    return selected_dicts

def select_top_n_dicts_for_all_files(n):
    selected_data = []

    # Parcourir tous les fichiers dans le répertoire
    for i in range(645):
        file_path = f'glevy/StatApp/chunk_{i}_shuffled_wikipedia_data.json'

        # Sélectionner les N dictionnaires pour chaque fichier
        selected_dicts = select_top_n_dicts(file_path, n)

        # Ajouter les résultats à la liste globale
        selected_data.extend(selected_dicts)

    return selected_data

# Exemple d'utilisation : sélectionner les 10 meilleurs dictionnaires pour chaque fichier
    # Ça a pris quelques minutes à peine, ça va (ressources CPU et RAM au max sur un pytorch simple)
# top_10_sur_10000 = select_top_n_dicts_for_all_files(10)

# Enregistrer la liste globale en local
# with fs.open('glevy/StatApp/top_10_sur_10000.json', 'w') as f:
#     json.dump(top_10_sur_10000, f)

# Réalisé avec N = 10, 100, 500, flemme de faire 1000 en vrai.