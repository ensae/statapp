# Vieille fonction probablement pas utile, archivée ici 'au cas où'.

def unit_replace(chunk, before, after):
    return Dataset.from_list(chunk).map(lambda x: {'text': x['text'].replace(before, after)})

def bulk_replace(chunk, list_before, list_after):
    assert len(list_before) == len(list_after) # Ce serait con quand même
    for b, a in zip(list_before, list_after):
        chunk = unit_replace(chunk, b, a)
    return chunk

LIST_BEFORE = ['\n\n', '\n']
LIST_AFTER = [' ', ' ']