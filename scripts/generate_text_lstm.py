# scripts/generate_text_lstm.py
import torch
from transformers import GPT2Tokenizer

def generate_text_lstm(model, tokenizer, seed_text, length):
    model_lstm.eval()

    input_ids_lstm = tokenizer.encode(seed_text, return_tensors="pt")

    for _ in range(length):
        with torch.no_grad():
            output_lstm = model_lstm(input_ids_lstm)

        next_token_lstm = torch.argmax(output_lstm[:, -1, :]).item()
        next_token_lstm = torch.tensor([[next_token_lstm]])
        input_ids_lstm = torch.cat([input_ids_lstm, next_token_lstm], dim=-1)

    generated_text_lstm = tokenizer.decode(input_ids_lstm[0], skip_special_tokens=True)
    return generated_text_lstm
