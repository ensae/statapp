# scripts/train_gpt2.py
from transformers import Trainer, TrainingArguments, GPT2LMHeadModel, GPT2Tokenizer
from datasets import load_dataset, DatasetDict

def train_gpt2():
    # Charger le tokenizer et le modèle GPT-2
    tokenizer = GPT2Tokenizer.from_pretrained('gpt2-medium')
    model_gpt2 = GPT2LMHeadModel.from_pretrained('gpt2-medium')

    # Charger les données (Wikipedia en anglais)
    wikipedia_data = load_dataset('wikipedia', '20220301.en')

    # Créer un ensemble de données (DatasetDict) pour l'entraînement
    data_all = DatasetDict(
        {
            "train": wikipedia_data["train"].shuffle().select(range(300)),
            "valid": wikipedia_data["train"].shuffle().select(range(300, 380, 1)),
        }
    )

    # Prétraiter les données pour l'entraînement du modèle GPT-2
    tokenized_datasets = data_all.map(
        lambda x: tokenizer(x['text'], truncation=True, max_length=128),
        batched=True,
        remove_columns=data_all["train"].column_names
    )

    # Configurer l'entraînement du modèle GPT-2
    args_gpt2 = {
        "output_dir": "Model_GPT2",
        "per_device_train_batch_size": 32,
        "per_device_eval_batch_size": 32,
        "evaluation_strategy": "steps",
        "eval_steps": 5_000,
        "logging_steps": 5_000,
        "gradient_accumulation_steps": 8,
        "num_train_epochs": 1,
        "weight_decay": 0.1,
        "warmup_steps": 1_000,
        "lr_scheduler_type": "cosine",
        "learning_rate": 5e-4,
        "save_steps": 5_000,
        "fp16": True,
        "push_to_hub": True,
        "hub_token": "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",
    }

    trainer_gpt2 = Trainer(
        model=model_gpt2,
        tokenizer=tokenizer,
        args=TrainingArguments(**args_gpt2),
        data_collator=DataCollatorForLanguageModeling(tokenizer, mlm=False),
        train_dataset=tokenized_datasets["train"],
        eval_dataset=tokenized_datasets["valid"]
    )

    # Entraîner le modèle GPT-2
    trainer_gpt2.train()
