# scripts/generate_text_gpt2.py
from transformers import GPT2LMHeadModel, GPT2Tokenizer
import torch
import torch.nn.functional as F

def generate_text_gpt2(model, tokenizer, seed_text, length, temperature=1.0):
    model.eval()

    input_ids = tokenizer.encode(seed_text, return_tensors="pt")

    for _ in range(length):
        with torch.no_grad():
            output = model(input_ids)

        logits_last_token = output[:, -1, :] / temperature
        probabilities_last_token = F.softmax(logits_last_token, dim=-1)

        next_token = torch.multinomial(probabilities_last_token, 1).item()
        next_token = torch.tensor([[next_token]])
        input_ids = torch.cat([input_ids, next_token], dim=-1)

    generated_text = tokenizer.decode(input_ids[0], skip_special_tokens=True)
    return generated_text
