# scripts/train_lstm.py
import torch
import torch.nn as nn
import torch.optim as optim
from transformers import GPT2Tokenizer

class TextGeneratorLSTM(nn.Module):
    def __init__(self, vocab_size, embed_size, hidden_size, num_layers):
        super(TextGeneratorLSTM, self).__init__()

        self.embedding = nn.Embedding(vocab_size, embed_size)
        self.lstm = nn.LSTM(embed_size, hidden_size, num_layers, batch_first=True)
        self.fc = nn.Linear(hidden_size, vocab_size)

    def forward(self, x):
        embedded = self.embedding(x)
        output, _ = self.lstm(embedded)
        output = self.fc(output)
        return output

def train_lstm(tokenizer, tokenized_datasets):
    # Paramètres du modèle LSTM
    vocab_size_lstm = len(tokenizer)
    embed_size_lstm = 256
    hidden_size_lstm = 512
    num_layers_lstm = 2

    # Création du modèle LSTM
    model_lstm = TextGeneratorLSTM(vocab_size_lstm, embed_size_lstm, hidden_size_lstm, num_layers_lstm)

    # Définition de la fonction de perte et de l'optimiseur pour le modèle LSTM
    criterion_lstm = nn.CrossEntropyLoss()
    optimizer_lstm = optim.Adam(model_lstm.parameters(), lr=0.001)

    # Données d'entraînement pour le modèle LSTM
    train_data_lstm = torch.tensor(tokenized_datasets['train']['input_ids'])
    train_dataset_lstm = torch.utils.data.TensorDataset(train_data_lstm)
    train_loader_lstm = torch.utils.data.DataLoader(train_dataset_lstm, batch_size=64, shuffle=True)

    # Boucle d'entraînement pour le modèle LSTM
    num_epochs_lstm = 10

    for epoch in range(num_epochs_lstm):
        model_lstm.train()

        for batch_data_lstm in train_loader_lstm:
            optimizer_lstm.zero_grad()
            output_lstm = model_lstm(batch_data_lstm[0])
            loss_lstm = criterion_lstm(output_lstm.view(-1, vocab_size_lstm), batch_data_lstm[0].view(-1))
            loss_lstm.backward()
            optimizer_lstm.step()

        print(f'Epoch {epoch + 1}/{num_epochs_lstm}, Loss: {loss_lstm.item()}')
