import s3fs

def upload_data(filepath, file_name):
    s3 = s3fs.S3FileSystem()
    s3_path = f"glevy/Modèles/Test/{file_name}"
    s3.put(filepath, s3_path, recursive=True)

def transfer_folder_to_s3(local_origin_folder, s3_dest_folder, recursive=True):
    fs.put(local_origin_folder, s3_dest_folder, recursive=True)