# script config_and_train_model_gpt

# Partie système et fichiers
import s3fs
import json
import os

# Partie traitement de données
import pandas as pd

# Le modèle et son entrainement
from datasets import load_dataset, Dataset, DatasetDict
from transformers import GPT2LMHeadModel, AutoConfig, AutoTokenizer
from transformers import Trainer, TrainingArguments, DataCollatorForLanguageModeling, pipeline
import torch


S3_ENDPOINT_URL = "https://" + os.environ["AWS_S3_ENDPOINT"]
fs = s3fs.S3FileSystem(client_kwargs={'endpoint_url': S3_ENDPOINT_URL})

def dataset_file_to_data_all(file='glevy/StatApp/top_10_sur_10000.json', train_ratio=0.8):

    with fs.open(file, 'r') as f:
        data = Dataset.from_list(json.load(f))
        
    size = len(data)
    size_train = int(size*8/10)
    
    data_all = DatasetDict(
    {
        "train": data.select(range(size_train)),  
        "valid": data.select(range(size_train,size)), 
    }
)
    return data_all


    

# Tokenize marche pas pour l'instant, à cause de tokenizer pas défini
def tokenize(element, context_length=128, tokenizer_engine="gpt2-medium"): 

    tokenizer = AutoTokenizer.from_pretrained('gpt2-medium')
    
    outputs = tokenizer(
        element['text'],
        truncation=True,
        max_length=context_length,
        return_overflowing_tokens=True,
        return_length=True,
    )
    input_batch = []
    for length, input_ids in zip(outputs["length"], outputs["input_ids"]):
        if length == context_length:
            input_batch.append(input_ids)
    return {'input_ids':input_batch} #, tokenizer (on l'enlève pour le moment)

def config_train_model_gpt(tokenized_datasets, name='test', tokenizer=None, n_embd=512, n_layer=8, n_head=8, GPU=True, push_hub=False, context_length=128, tokenizer_engine='gpt2-medium', model_engine='gpt2', fs=None, USER=None):
    
    assert fs is not None # Sinon on ne sauvegarde rien et c'est très con.
    
    config = AutoConfig.from_pretrained(
    model_engine,
    vocab_size=len(tokenizer),
    n_embd= n_embd,
    n_layer= n_layer,
    n_head= n_head,
    n_ctx=context_length,
    bos_token_id=tokenizer.bos_token_id,
    eos_token_id=tokenizer.eos_token_id
    )
    
    model = GPT2LMHeadModel(config)
    
    print('Nombre de poids dans le modèle : ' + str(model.num_parameters())) 
    # Juste pour être sûr, avec une dépendance non linéaire en les arguments.
    
    tokenizer.pad_token = tokenizer.eos_token
    data_collator = DataCollatorForLanguageModeling(tokenizer, mlm=False)
    
    args = TrainingArguments(
    output_dir= None,    
    per_device_train_batch_size=32,          
    per_device_eval_batch_size=32,          
    evaluation_strategy="steps",
    eval_steps=5_000,
    logging_steps=5_000,
    gradient_accumulation_steps=8,
    num_train_epochs=3,
    weight_decay=0.1,
    warmup_steps=1_000,
    lr_scheduler_type="cosine",
    learning_rate=5e-4,
    save_steps=5_000,
    fp16= GPU,  # Si GPU == False, c'est horriblement lent.
    )
        
    trainer = Trainer(
    model=model,
    tokenizer=tokenizer,
    args=args,
    data_collator=data_collator,
    train_dataset=tokenized_datasets["train"],
    eval_dataset=tokenized_datasets["valid"])

    # La partie chronophage
    trainer.train()
    
    # Parce que oui, entraîner sans sauvegarder c'est pas malin.
    model.save_pretrained(f'./saved_model_{name}')
    tokenizer.save_pretrained(f'./saved_model_{name}')
    
    # Parce que non, on ne gitte PAS par paquets de 500 Mo c'est niet.
    fs.put(f'./saved_model_{name}', f'{USER}/Modèles/{name}', recursive=True)

    return pd.DataFrame(trainer.state.log_history)

   






 


    
    