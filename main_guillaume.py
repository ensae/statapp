# main.py
from scripts.train_gpt2 import train_gpt2
from scripts.generate_text_gpt2 import generate_text_gpt2
from scripts.train_lstm import train_lstm
from scripts.generate_text_lstm import generate_text_lstm
from transformers import GPT2Tokenizer

def main_guillaume():
    # Appeler les différentes fonctions en fonction des besoins
    train_gpt2()
    generate_text_gpt2(model_gpt2, tokenizer, "Once upon a time", length=50, temperature=0.8)
    train_lstm(tokenizer, tokenized_datasets)
    generate_text_lstm(model_lstm, tokenizer, "Once upon a time", length=50)